package com.example.tom.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText edtN1 = (EditText) findViewById(R.id.edtN1);
        final EditText edtN2 = (EditText) findViewById(R.id.edtN2);
              Button btSum = (Button) findViewById(R.id.btMais);
              Button btSub = (Button) findViewById(R.id.btMenos);
              Button btMult = (Button) findViewById(R.id.btVezes);
              Button btDiv = (Button) findViewById(R.id.btDividir);


        btSum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

               float result =  Float.parseFloat(edtN1.getText().toString()) + Float.parseFloat(edtN2.getText().toString());
                Toast.makeText(getApplicationContext(),"soma = "+ result ,Toast.LENGTH_LONG).show();
            }
        });

        btSub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                float result =  Float.parseFloat(edtN1.getText().toString()) - Float.parseFloat(edtN2.getText().toString());
                Toast.makeText(getApplicationContext(),"soma = "+ result ,Toast.LENGTH_LONG).show();
            }
        });

        btMult.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                float result =  Float.parseFloat(edtN1.getText().toString()) * Float.parseFloat(edtN2.getText().toString());
                Toast.makeText(getApplicationContext(),"soma = "+ result ,Toast.LENGTH_LONG).show();
            }
        });

        btDiv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                float result =  Float.parseFloat(edtN1.getText().toString()) / Float.parseFloat(edtN2.getText().toString());
                Toast.makeText(getApplicationContext(),"soma = "+ result ,Toast.LENGTH_LONG).show();
            }
        });
    }
}
